package Main;

import java.awt.*;
import javax.swing.JPanel;
import java.awt.event.*;
import java.awt.image.BufferedImage;

public class GamePanel extends JPanel implements KeyListener{
	public static final int WIDTH = 160;
	public static final int HEIGHT = 320;
	public static final int SCALE = 2;
	
	public int[][] grid = new int[10][17];
	public Block b;
	public Graphics g;
	public BufferedImage image;
	
	public GamePanel() {
		super();
		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setFocusable(true);
		requestFocus();
		init();
	}

	public void init() {
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = image.getGraphics();
		
		b = new Block();
		b.x = 80;
		b.draw(g);
		}
		
	public void keyPressed(KeyEvent k) {
		 if (k.getKeyCode() == KeyEvent.VK_LEFT) {
		      if (b.x > 0) {
		        b.x -= 16;
		      }
		    } 
		 else if (k.getKeyCode() == KeyEvent.VK_RIGHT) {
		      if (b.x < 144) {
		        b.x += 16;
		        }
		 }
	}

	public void keyReleased(KeyEvent k) {}

	public void keyTyped(KeyEvent k) {}
}
